# FAQ

-	[The icon does not work. How to enable it ?](#the-icon-does-not-work-how-to-enable-it)
-	[How-to customize Windows (un)installation (using NSIS) ?](how-to-customize-Windows-un-installation-using-NSIS)

## The icon does not work. How to enable it ?

PwSh.Fw.BuildHelpers use [Image Magick](https://imagemagick.org/) and [icnsutils](https://icns.sourceforge.io/) to automate icon conversion.
Icon is handle automatically by the `IconFile` attribute in your `project.yml`.

Image Magick is used to resize icons for linux desktop and to convert icon to `ico` file format for windows.

ICNS is used to convert icon to `icns` file format used by macOS.

To make it work, install Image Magick
-	if your are on Windows, please install last version of [Image Magick](https://imagemagick.org/) AND make sure to check `[ ] Install legacy tools (convert,...)` during installation process
-	if you are on Linux, install it via your package manager. It is likely called `imagemagick`.
-	if you are on macOS, you can install it with `brew` with command `brew install imagemagick`.

Please refer to the [Image Magick download page](https://imagemagick.org/script/download.php) to read specific instructions.

The install icns-utils :
-	TODO : windows ?
-	if you are on Linux, install it via your package manager. It is likely called `icnsutils`.
-	TODO : macOS ?

Then create an image, preferably a `PNG` with a sufficient big size (512x512 is ok for the time being). Say you put it under `src/images` of your project and you called it `myIcon.png`.

Modify your `project.yml` as follow :

```yml
IconFile: src/images/myIcon.png
```

Now, you can build a debian package, a windows installer or a macos package, your icon will fit right where it is needed.

## How-to customize Windows (un)installation (using NSIS) ?

Process to create NSIS Windows installer contains multiple hooks to let user customize the installation process.

While building the installer program, the `Install` section have 2 hooks :
-	a pre hook is included early in the `Install` section. It must be named `nsis_install_pre.nsh` and it must be located in the folder you pass to the `New-NSISBuild -WindowsFolder` parameter.
-	a post hook is included at the end of the `Install` section, just before writing the uninstaller program. It must be named `nsis_install_post.nsh` and it must be located in the folder you pass to the `New-NSISBuild -WindowsFolder` parameter.

Additionally, the setup can launch a `postinst.ps1` when installing the program. The `postinst.ps1` must be located in the folder you pass to the `New-NSISBuild -WindowsFolder` parameter. It will be included in the resulting setup.exe.

The same hooks apply to uninstaller :
-	a pre hook is included early in the `Uninstall` section. It must be named `nsis_uninstall_pre.nsh` and it must be located in the folder you pass to the `New-NSISBuild -WindowsFolder` parameter.
-	a post hook is included at the end of the `Uninstall` section, just before writing the uninstaller program. It must be named `nsis_uninstall_post.nsh` and it must be located in the folder you pass to the `New-NSISBuild -WindowsFolder` parameter.
