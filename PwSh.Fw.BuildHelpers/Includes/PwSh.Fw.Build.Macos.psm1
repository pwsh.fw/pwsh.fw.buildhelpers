# @var		ConvertIsInstalled
# @brief	$true if 'convert' from Image Magick package is installed
$Script:ConvertIsInstalled = Get-Command -Name "convert" -ErrorAction SilentlyContinue
$Script:ConvertNotInstalledMessage = "'convert' command not found. Automatic icon handling is disabled. Please read the FAQ if you need it."
$Script:Png2icnsIsInstalled = Get-Command -Name "png2icns" -ErrorAction SilentlyContinue
$Script:Png2icnsNotInstalledMessage = "'png2icns' command not found. Automatic icon handling is disabled. Please read the FAQ if you need it."

<#
.SYNOPSIS
Convert an image to icns Apple format

.DESCRIPTION
Convert an image to icns format to be used on macOS system.

.PARAMETER Image
Full path to an image file

.PARAMETER Destination
Destination folder

.PARAMETER Filename
Optional. New filename of the image

.EXAMPLE
ConvertTo-LinuxIcons -Image /path/to/favicon.png

.NOTES
This function do not convert image size. It just convert format.
#>
function ConvertTo-MacOSIcons {
	[CmdletBinding()]
	[OutputType([string], [Boolean])]
	Param (
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$Image,
		[Parameter(Mandatory = $false, ValueFromPipeLine = $false)][string]$Destination,
		[Parameter(Mandatory = $false, ValueFromPipeLine = $false)][string]$Filename
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		if (!(Test-FileExist $Image)) {
			eerror "Image '$Image' not found."
			$rc = $false
		}
		$basename = (Get-Item $Image).BaseName
		# $ext = (Get-Item $Image).Extension
		if ([string]::IsNullOrEmpty($Filename)) {
			$Filename = $basename
		}
		if ([string]::IsNullOrEmpty($Destination)) {
			$Destination = (Get-Item $Image).DirectoryName
		}

		if ($Script:Png2icnsIsInstalled) {
			$null = Execute-Command -exe "png2icns" -args "$Destination/$Filename.icns $Image"
			$rc = "$Destination/$Filename"
		} else {
			Write-Warning $Script:Png2icnsNotInstalledMessage
			$rc = $Image
		}

		return $rc
	}

	End {
		Write-LeaveFunction
	}
}
