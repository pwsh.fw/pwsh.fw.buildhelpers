using namespace System.Management.Automation

<#
	.SYNOPSIS Class used to Autocomplete and validate Platform parameter
#>
class PwShFwSupportedPlatforms : IValidateSetValuesGenerator {

	static $ValidValues = @('Linux', 'macOS', 'Windows')

	## GetValidValues
	## @brief	used by ValidateSet parameter validator
    [string[]] GetValidValues() {
        # $Values = $this.ValidValues
        return [PwShFwSupportedPlatforms]::ValidValues
    }

	## GetValues
	## @brief	static function used in code
    static [string[]] GetValues() {
        # $Values = @('Linux', 'macOS', 'Windows')
        return [PwShFwSupportedPlatforms]::ValidValues
    }
}

<#
	.SYNOPSIS Class used to Autocomplete and validate Architecture parameter
#>
class PwShFwSupportedArchitectures : IValidateSetValuesGenerator {

	static $ValidValues = @('all', 'arm32', 'arm64', 'x86', 'x64')

	## GetValidValues
	## @brief	used by ValidateSet parameter validator
    [string[]] GetValidValues() {
        # $Values = $this.ValidValues
        return [PwShFwSupportedArchitectures]::ValidValues
    }

	## GetValues
	## @brief	static function used in code
    static [string[]] GetValues() {
        # $Values = @('Linux', 'macOS', 'Windows')
        return [PwShFwSupportedArchitectures]::ValidValues
    }
}

<#
	.SYNOPSIS Class used to Autocomplete and validate Hash parameter
#>
class PwShFwValidHashes : IValidateSetValuesGenerator {

	static $ValidValues = @('MD5', 'SHA1', 'SHA256')

	## GetValidValues
	## @brief	used by ValidateSet parameter validator
    [string[]] GetValidValues() {
        # $Values = $this.ValidValues
        return [PwShFwValidHashes]::ValidValues
    }

	## GetValues
	## @brief	static function used in code
    static [string[]] GetValues() {
        # $Values = @('Linux', 'macOS', 'Windows')
        return [PwShFwValidHashes]::ValidValues
    }
}

<#
	.SYNOPSIS Class used to Autocomplete and validate License parameter
#>
class PwshFwSupportedLicenses : IValidateSetValuesGenerator {

	static $ValidValues = (Invoke-RestMethod https://gitlab.com/api/v4/templates/licenses).key

	## GetValidValues
	## @brief	used by ValidateSet parameter validator
    [string[]] GetValidValues() {
        # $Values = $this.ValidValues
        return [PwshFwSupportedLicenses]::ValidValues
    }

	## GetValues
	## @brief	static function used in code
    static [string[]] GetValues() {
        # $Values = @('Linux', 'macOS', 'Windows')
        return [PwshFwSupportedLicenses]::ValidValues
    }
}

# EXAMPLE
# function Clear-FileInCurrentLocation {
#     [CmdletBinding(SupportsShouldProcess, ConfirmImpact = 'High')]
#     param(
#         [Parameter(Position = 0, Mandatory)]
#         [ValidateSet( [PwShFwSupportedPlatforms] )]
#         [string]
#         $Path
#     )

#     Clear-Content -Path $Path
# }

