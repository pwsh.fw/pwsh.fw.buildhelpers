$CommonStartMenu = Get-ItemPropertyValue "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" -Name "Common Start Menu"

function New-WindowsShortcut {
	[CmdletBinding()]
	[OutputType([String])]
	Param (
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$Target,
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$Destination,
		[Parameter(Mandatory = $true, ValueFromPipeLine = $true)][string]$Name,
		[Parameter(Mandatory = $false, ValueFromPipeLine = $true)][string]$Icon,
		[Parameter(Mandatory = $false, ValueFromPipeLine = $true)][string]$Args
	)
	Begin {
		Write-EnterFunction
	}

	Process {
		$null = New-Item $Destination -Force -ItemType Directory
		$SourceFileLocation = $Target
		$ShortcutLocation = "$Destination\$Name"
		$WScriptShell = New-Object -ComObject WScript.Shell
		$Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)
		$Shortcut.TargetPath = $SourceFileLocation
		if ($Icon) { $Shortcut.IconLocation = $Icon }
		if ($Args) { $Shortcut.Arguments = $Args }
		$Shortcut.Save()

		return "$Destination\$Name"
	}

	End {
		Write-LeaveFunction
	}
}
