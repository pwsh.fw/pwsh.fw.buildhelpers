# setup.nsi template file
# @see @url https://nsis.sourceforge.io/Docs/
#
#Build Switches
# /DVERSION=[Version]

# Switches:
# /INI [IniFile] Use settings from INI file
# /S Silent

# Exit codes:
# 0: OK
# 1: Cancel
# 2: Not administrator
# -1: Error

#
# Includes
#
!include FileFunc.nsh
!include LogicLib.nsh
!include x64.nsh
!insertmacro GetParameters
!insertmacro GetOptions

#
# defines
#
; !include .\header.nsi

#
# General Attributes
#
Unicode true
CrcCheck off # CRC check generates random errors
Icon "${ICONFILE}"
Name "${NAME}"
; OutFile "${ROOT}\releases\${NAME}-${VERSION}.exe"
RequestExecutionLevel admin

VIAddVersionKey "ProductName" "${DisplayName}"
VIAddVersionKey "Comments" "${DESCRIPTION}"
VIAddVersionKey "CompanyName" "${COMPANYNAME}"
VIAddVersionKey "LegalCopyright" "${COPYRIGHT}"
VIAddVersionKey "FileDescription" "Installer"
VIAddVersionKey "FileVersion" "${VERSION}"
VIProductVersion "${VERSION}"
;VIProductVersion "1.0.0.0"

!ifdef NAMESPACE
	InstallDir "$PROGRAMFILES64\${NAMESPACE}\${NAME}"
	InstallDirRegKey HKLM Software\${NAMESPACE}\${NAME} InstallDir
	!define PRODUCT_UNINST_KEY "Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\${NAMESPACE}_${NAME}"
!else
	InstallDir "$PROGRAMFILES64\${NAME}"
	InstallDirRegKey HKLM Software\${NAME} InstallDir
	!define PRODUCT_UNINST_KEY "Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\${NAME}"
!endif

!ifdef PRERELEASE
	!define SCRIPTS_SUFFIX "_prerelease"
!else
	!define SCRIPTS_SUFFIX ""
!endif

#
# Pages
#
Page license
Page directory
Page instfiles
UninstPage uninstConfirm
UninstPage instfiles

LicenseData "${LICENSEFILE}"

#
# Functions
#
Function Usage
  push $0
  StrCpy $0 "Switches:$\r$\n"
  StrCpy $0 "$0/S - Install ${NAME} silently with no user prompt.$\r$\n"
  StrCpy $0 "$0/D=c:\path\to\install\folder - Specify an alternate installation folder. Default install dir is '$PROGRAMFILES64'.$\r$\n"
  MessageBox MB_OK $0
  pop $0
FunctionEnd

Function .onInit
  ${GetParameters} $R0
  ClearErrors
  ${GetOptions} $R0 "/?"    $R1
  ${IfNot} ${Errors}
    call Usage
    Abort
  ${EndIf}
  # use HKLM\Software and C:\Program Files, even on 64-bit computer
  # where Windows would normally redirect ourself to
  # HKLM\Software\wow6432Nodes and C:\Program Files (x86)
  ${If} ${RunningX64}
	DetailPrint "Running on a 64-bit Windows... setting RegView accordingly"
	SetRegView 64
  ${Else}
	DetailPrint "Running on a 32-bit Windows..."
  ${EndIf}
  # By default, we use the 'All Users' context
  SetShellVarContext all
FunctionEnd

#
# Sections
#
; ; The "" makes the section hidden.
; Section "" SecUninstallPrevious
    ; Call UninstallPrevious
; SectionEnd

Section "Install"
	SetOutPath "$INSTDIR"

	; include project's pre hook
	!include /NONFATAL "${USER_BUILD_WINDOWS_FOLDER}\nsis_install_pre${SCRIPTS_SUFFIX}.nsh"

	; pack everything
	File /r /x "*.bak" "${SOURCE}\*"

	; write registry values
	!ifdef NAMESPACE
		WriteRegStr HKLM "Software\${NAMESPACE}\${NAME}" "InstallDir" "$INSTDIR"
		WriteRegStr HKLM "Software\${NAMESPACE}\${NAME}" "version" "${VERSION}"
	!else
		WriteRegStr HKLM "Software\${NAME}" "InstallDir" "$INSTDIR"
		WriteRegStr HKLM "Software\${NAME}" "version" "${VERSION}"
	!endif
	; add/remove programs
	DetailPrint "Registering uninstallation options in add/remove programs"
	WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "DisplayName" "${DisplayName}"
	; if we omit *UninstallString, it will not display in Add/Remove Programs
	WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "UninstallString" '"$INSTDIR\uninst.exe"'
	WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "QuietUninstallString" '"$INSTDIR\uninst.exe" /S'
	WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "InstallLocation" "$INSTDIR"
	WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\${ICONBASENAME}"
	WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "Publisher" "${COMPANYNAME}"
	WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PROJECTURI}"
	WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${VERSION}"
	WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "NoModify" 1
	WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "NoRepair" 1
	; from @url http://nsis.sourceforge.net/Add_uninstall_information_to_Add/Remove_Programs
	${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
	IntFmt $0 "0x%08X" $0
	WriteRegDWORD HKLM "${PRODUCT_UNINST_KEY}" "EstimatedSize" "$0"

	; include project's post hook
	!include /NONFATAL "${USER_BUILD_WINDOWS_FOLDER}\nsis_install_post${SCRIPTS_SUFFIX}.nsh"

	; run post-install script provided by developer
	IfFileExists "$INSTDIR\Windows\postinst.ps1" postinst skip_postinst
	postinst:
	DetailPrint "Running post-install script"
	nsExec::ExecToLog `powershell.exe -ExecutionPolicy bypass -Command "& '$INSTDIR\Windows\postinst.ps1' -Configure"`
	nsExec::ExecToLog `pwsh.exe -ExecutionPolicy bypass -Command "& '$INSTDIR\Windows\postinst.ps1' -Configure"`
	skip_postinst:

	WriteUninstaller $INSTDIR\uninst.exe
SectionEnd

Section "Uninstall"

	; include project's pre hook
	!include /NONFATAL "${USER_BUILD_WINDOWS_FOLDER}\nsis_uninstall_pre${SCRIPTS_SUFFIX}.nsh"

	# use HKLM\Software and C:\Program Files, even on 64-bit computer
	# where Windows would normally redirect ourself to
	# HKLM\Software\wow6432Nodes and C:\Program Files (x86)
	${If} ${RunningX64}
		DetailPrint "Running on a 64-bit Windows... setting RegView accordingly"
		SetRegView 64
	${Else}
		DetailPrint "Running on a 32-bit Windows..."
	${EndIf}

	Delete "$INSTDIR\uninst.exe"
	RMDIR /r "$INSTDIR"
	DeleteRegKey HKLM "${PRODUCT_UNINST_KEY}"

	; include project's post hook
	!include /NONFATAL "${USER_BUILD_WINDOWS_FOLDER}\nsis_uninstall_post${SCRIPTS_SUFFIX}.nsh"

	; run post-uninstall script provided by developer
	IfFileExists "$INSTDIR\Windows\postrm.ps1" postrm skip_postrm
	postrm:
	DetailPrint "Running post-uninstall script"
	nsExec::ExecToLog `powershell.exe -ExecutionPolicy bypass -Command "& '$INSTDIR\Windows\postrm.ps1' -Remove"`
	nsExec::ExecToLog `pwsh.exe -ExecutionPolicy bypass -Command "& '$INSTDIR\Windows\postrm.ps1' -Remove"`
	skip_postrm:

SectionEnd
