[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force
)

$DS = [io.path]::DirectorySeparatorChar
$ROOTDIR = (Resolve-Path $PSScriptRoot/../).Path.TrimEnd(${DS})
Write-Output ">> header"

Write-Output "ROOTDIR = $ROOTDIR"

if (-not ($SkipNuGet)) {
	if (!(Get-PackageProvider -Name NuGet)) { Install-PackageProvider -Name NuGet -Force -Confirm:$false }
	Write-Output "Get-PSRepository ----"
	Get-PSRepository
	Write-Output "Get-PSRepository ----"
	$PSGallery = Get-PSRepository -Name PSGallery
	if (!($PSGallery)) {
		Register-PSRepository -Default -InstallationPolicy Trusted
	} else {
		Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
	}
	Install-Module PwSh.Fw.Core -Repository PSGallery -ErrorAction stop -Force:$Force
	Install-Module PlatyPS -confirm:$false -force:$Force
	Install-Module powershell-yaml -confirm:$false -force:$Force
}
Import-Module PlatyPS -force:$Force -ErrorAction stop
Import-Module powershell-yaml -force:$Force -ErrorAction stop
Import-Module PwSh.Fw.Core -force:$Force -ErrorAction stop

$Global:QUIET = $false
$Global:INFO = $true
$Global:VERBOSE = $true
$Global:DEBUG = $true
$Global:DEVEL = $true
$Global:TRACE = $true

Get-Module -Name PwSh.Fw.* -ListAvailable | format-table -AutoSize
$PSVersionTable

# only for PwSh.Fw.BuildHelpers project
Import-Module $ROOTDIR/PwSh.Fw.BuildHelpers/PwSh.Fw.BuildHelpers.psm1 -Force
$project = Get-Project -Path $ROOTDIR
[PSCustomObject]$project | Sort-ByProperties
# $project

Write-Host -ForegroundColor Blue "`n<< header"
