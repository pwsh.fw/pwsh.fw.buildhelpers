[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force
)

. $PSScriptRoot/00_header.ps1 -SkipNuGet:$SkipNuGet -Force:$Force

# $ROOTDIR = (Resolve-Path $PSScriptRoot/../).Path
$BASENAME = Split-Path -Path $PSCommandPath -Leaf
Write-Host -ForegroundColor Blue ">> $BASENAME"

# files and folder before
Get-ChildItem $ROOTDIR/$($project.Name) -Recurse
# only for PwSh.Fw.BuildHelpers project
Import-Module $ROOTDIR/PwSh.Fw.BuildHelpers/Includes/PwSh.Fw.Build.Powershell.psm1 -Force
# Update-ModuleManifestRecurse -FullyQualifiedName $ROOTDIR/$($project.Name)/$($project.Name).psm1 -Metadata $project -Recurse
Update-SingleModuleManifest -FullyQualifiedName $ROOTDIR/$($project.Name)/$($project.Name).psm1 -Metadata $project
# files and folder after
Get-ChildItem $ROOTDIR/$($project.Name) -Recurse
Import-Module $ROOTDIR/$($project.Name)/$($project.Name).psd1 -Force
Get-Module $($project.Name) | Format-Table Name, Version, ExportedFunctions
# remove all manifest
# Get-ChildItem -Path $ROOTDIR/$($project.Name) -Recurse -Filter "*.psd1" | Remove-Item
# New-NSISBuild -Project $project -Source $ROOTDIR/PwSh.Fw.BuildHelpers -Destination $ROOTDIR/releases -Verbose:$Verbose -Debug:$Debug -Confirm:$false

Write-Host -ForegroundColor Blue "<< $BASENAME"
