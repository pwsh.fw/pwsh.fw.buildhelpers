[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force,
	[string]$Destination = "$ROOTDIR/public"
)

. $PSScriptRoot/00_header.ps1 -SkipNuGet:$SkipNuGet -Force:$Force

$BASENAME = Split-Path -Path $PSCommandPath -Leaf
Write-Host -ForegroundColor Blue ">> $BASENAME"

if (-not ($SkipNuGet)) {
	Install-Module PlatyPS -AllowPrerelease -Scope CurrentUser -AcceptLicense -ErrorAction SilentlyContinue
}

Import-Module platyPS
$null = Remove-Item $Destination -Recurse -Force:$Force
$null = New-Item -Path $Destination -ItemType Directory
# Module definition files are build at 03_build time
# load common.class.ps1 because it seems to not be loaded automatically
. $ROOTDIR/$($project.Name)/Classes/common.class.ps1
foreach ($module in Get-ChildItem *.psm1 -recurse) {
	Import-Module -FullyQualifiedName $module.fullname -Force
	if (($module.psParentpath -split('/'))[-1] -eq $($module.BaseName)) {
		$Path = $Destination
	} else {
		$null = New-Item -Path "$Destination/$($module.BaseName)" -ItemType Directory
		$Path = "$Destination/$($module.BaseName)"
	}
	New-MarkdownHelp -Module $($module.BaseName) -OutputFolder "$Path"
}