[CmdletBinding()]Param(
)

Write-Info ">> Init"
Install-Module -Name platyPS -Confirm:$false
Import-Module platyPS -ErrorAction stop
Install-Module powershell-yaml -Confirm:$false
Import-Module powershell-yaml -ErrorAction stop

$PSVersionTable
Set-Variable -Name VERSION -Value (Get-Content $PSScriptRoot/../VERSION)
Write-Output "VERSION = $VERSION"
$project = Get-Content $PSScriptRoot/../project.yml -Raw | ConvertFrom-Yaml
$project | Format-Table Name, Value

Write-Info ">> Build help"
# git clone https://gitlab-ci-token:${CI_BUILD_TOKEN}@gitlab.com/PwSh.Fw/$($project.Name).wiki.git $PSScriptRoot/../../$($project.Name).wiki
if (-not (Test-Path $PSScriptRoot/../../$($project.Name).wiki)) {
	git clone git@gitlab.com:pwsh.fw/$($project.Name).wiki.git $PSScriptRoot/../../$($project.Name).wiki
}
if (Test-Path $PSScriptRoot/../../$($project.Name).wiki) {
	Push-Location $PSScriptRoot/../../$($project.Name).wiki
	Write-Verbose "PWD = $(Get-Location)"
	git pull
	Pop-Location
	Get-ChildItem -Path $PSScriptRoot/.. -Recurse -Name "*.psm1" | ForEach-Object {
		Write-Debug "Processing $_"
		$file = Get-Item $PSScriptRoot/../$_ -ErrorAction stop
		Import-Module -FullyQualifiedName $file.FullName
		New-MarkdownHelp -Module $($file.BaseName) -OutputFolder "$PSScriptRoot/../../$($project.Name).wiki/References/$($file.BaseName)" -Force
	}
	Push-Location $PSScriptRoot/../../$($project.Name).wiki
	Update-MarkdownHelp "./References"
	# git push https://gitlab-ci-token:${CI_BUILD_TOKEN}@gitlab.com/PwSh.Fw/$($project.Name).wiki.git
	git add References/*
	# git commit -am "wiki: update auto-generated documentation"
	# git push
	Pop-Location
	Write-Verbose "PWD = $(Get-Location)"
} else {
	Write-Error "Path '$PSScriptRoot/../../$($project.Name).wiki' not found. An error occured."
}

Write-Info "Done."
