[CmdletBinding()]Param(
)

. $PSScriptRoot/00_header.ps1

$ROOTDIR = (Resolve-Path $PSScriptRoot/../).Path
$BASENAME = Split-Path -Path $PSCommandPath -Leaf
Write-Host -ForegroundColor Blue ">> $BASENAME"

# only for PwSh.Fw.BuildHelpers project
Import-Module $ROOTDIR/PwSh.Fw.BuildHelpers/Includes/PwSh.Fw.Build.Powershell.psm1 -Force
New-NSISBuild -Project $project -Source $ROOTDIR/PwSh.Fw.BuildHelpers -Destination $ROOTDIR/releases

Write-Host -ForegroundColor Blue "<< $BASENAME"
