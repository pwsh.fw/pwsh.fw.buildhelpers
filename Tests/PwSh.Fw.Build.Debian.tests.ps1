$ROOTDIR = (Resolve-Path $PSScriptRoot/../).Path
$BASENAME = Split-Path -Path $PSCommandPath -Leaf
$Project = Get-Content "$ROOTDIR/project.yml" -Raw | ConvertFrom-Yaml
$ModuleName = $BASENAME -replace ".tests.ps1"

$null = Import-Module -FullyQualifiedName $ROOTDIR/$($Project.Name)/Includes/$ModuleName.psm1 -Force -PassThru -ErrorAction stop

if ((Get-Module Pester).Version -lt [version]'4.0.0') { throw "Pester 4.0.0 or later is required." }

Write-Output "PSScriptRoot = $PSScriptRoot"
Write-Output "ROOTDIR = $ROOTDIR"
Write-Output "BASENAME = $BASENAME"
Write-Output "ModuleName = $ModuleName"

Mock Write-Error { } -ModuleName $ModuleName
Mock Write-Host { } -ModuleName $ModuleName

Describe 'PwSh.Fw.Build.Debian' {

	Context 'Build.Debian' {

		$TMP = [io.path]::GetTempPath()
		$Metadata = $Project
		$Metadata.Package = $Project.name
		$Metadata.Version = "1.0.0"
		$Metadata.Section = "utils"
		$Metadata.Priority = "normal"
		$Metadata.ProcessorArchitecture = "x64"
		$Metadata.Architecture = "x64"
		$Metadata.Arch = "x64"
		$Metadata.Essential = "False"
		$Metadata.'Pre-Depends' = "False"
		$Metadata.Recommends = "git"
		$Metadata.Suggests = "git"
		$Metadata.Breaks = "pwshfw"
		$Metadata.Conflicts = "pwshfw"
		$Metadata.Provides = "PwSh.Fw.BuildHelpers"
		$Metadata.Replaces = "pwshfw"
		$Metadata.Enhances = "PwSh.Fw.Core"
		$Metadata.Size = 123456
		$Metadata.'Installed-Size' = 123456
		$Metadata.Author = $Project.Owner
		$Metadata.Maintainer = $Project.Owner
		$Metadata.ProjectUri = $Project.ProjectUrl

		It 'ConvertTo-DebianCONTROLFileSettings' {
			$cf = $Metadata | ConvertTo-DebianCONTROLFileSettings
			$cf.Package | Should -BeExactly $Project.Name
		}

		It 'Out-DebianCONTROLFile return path' {
			Remove-Item $TMP/DEBIAN -Force -Recurse -ErrorAction SilentlyContinue
			$control = $Metadata | Out-DebianCONTROLFile -Destination $TMP/DEBIAN
			(Resolve-Path -Path $control).Path | Should -BeExactly (Resolve-Path -Path $("$TMP/DEBIAN/control")).Path
			Test-Path -Path $control -PathType Leaf | Should -BeTrue
		}

		It 'Out-DebianCONTROLFile return object' {
			$control = $Metadata | Out-DebianCONTROLFile -Destination $TMP/DEBIAN -PassThru
			$control.Package | Should -BeExactly $Project.name
			$control.Name | Should -BeNullOrEmpty
		}

	}
}
