# Invoke code coverage
# Invoke-Pester -Script ./PwSh.Fw.BuildHelpers.tests.ps1 -CodeCoverage ../PwSh.Fw.BuildHelpers/PwSh.Fw.BuildHelpers.psm1

$BASENAME = Split-Path -Path $PSCommandPath -Leaf
$ModuleName = $BASENAME -replace ".tests.ps1"

# load header
. $PSScriptRoot/header.inc.ps1

Import-Module -FullyQualifiedName $ROOTDIR/$ModuleName/$ModuleName.psm1 -Force -ErrorAction stop

Describe 'PwSh.Fw.BuildHelpers' {

	Mock Write-Host { } -ModuleName $ModuleName
	$TMP = [io.path]::GetTempPath()
	Context 'New-ProjectFile' {

		It 'New-ProjectFile create project.yml' {
			$file = New-ProjectFile -Path $TMP
			(Resolve-Path $file).Path | Should -BeExactly (Resolve-Path "$($TMP)project.yml").Path
			Test-Path $file -PathType Leaf | Should -BeTrue
		}

		It 'New-ProjectFile return object' {
			$obj = New-ProjectFile -Path $TMP -PassThru
			$obj.name | Should -BeExactly "name"
		}

		It 'New-ProjectFile return $false' {
			$obj = New-ProjectFile -Path c:\this\path\does\not\exist
			$obj | Should -BeFalse
		}

		It 'Test-ProjectStructure return $true' {
			$rc = Test-ProjectStructure -Path $ROOTDIR
			$rc | Should -BeTrue
		}

		It 'Test-ProjectStructure return $false' {
			$rc = Test-ProjectStructure -Path c:\
			$rc | Should -BeFalse
		}

	}

	Context 'Get-ProjectChangelog' {

		It 'Get-ProjectChangelog return null if wrong file' {
			$rc = Get-ProjectChangelog -Path $TMP/changelog
			$rc | Should -BeNullOrEmpty
		}

		It 'Get-ProjectChangelog return hashtable' {
			$rc = Get-ProjectChangelog -Path $ROOTDIR/CHANGELOG.md
			$rc | Should -BeOfType hashtable
		}
	}

	Context 'Build' {

		It 'Initialize-BuildTree return $true' {
			$rc = Initialize-BuildTree -Map @{ "PwSh.Fw.BuildHelpers" = "Modules/PwSh.Fw.BuildHelpers" } -Source $ROOTDIR -Destination TestDrive:\dist
			$rc | should -BeTrue
			Test-Path -Path TestDrive:\dist\Modules\PwSh.Fw.BuildHelpers | Should -BeTrue
		}

	}

}
