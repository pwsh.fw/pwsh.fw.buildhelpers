# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.7.0] - 2024-06-20

### Added

- `Get-Project`: added parameter `-File` to be able to specify an alternate project file with alternate attributes
- `NSIS`: execute developer provided `postrm.ps1` script
- `NSIS`: execute `postinst.ps1` and `postrm.ps1` scripts with both `powershell.exe` and `pwsh.exe` (if available)
- `New-DebianBuild`: preprocess files in DEBIAN folder with `.mustache` extension into the PSMustache engine
- `New-DebianBuild`: can use wsl.exe on windows
- `New-NSISBuild`: preprocess files in WINDOWS folder with `.mustache` extension into the PSMustache engine
- `New-CabinetFile`: can use `lcab` on linux (not tested)

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [1.6.0] - 2023-05-23

### Added

-	`BUILD` can be set by environment variable, but can not be forced (e.g. will be overridden by pipeline BUILD number). Useful to test script outside of a pipeline.
-	new `Add-ToBuildTree()` function to add data when `Initialize-BuildTree` has been already called
-	new `Update-SingleModuleManifest` and `Update-MultipleModuleManifest` to replace bad written `Update-MetaModuleManifest`

### Changed

### Deprecated

### Removed

### Fixed

-	fixed issue caused by Import-Module not honoring PSModulePath
-	fixed `Update-ModuleManifestEx` returning extra data that prevented `Update-MetaModuleManifest` to work properly
-	fixed `New-Project` that created empty `project.yml` file
-	fixed `Update-ModuleManifestEx` not honoring NestedModules with `Includes` subfolders
-	fixed `fqVersion` for pre-released projects

### Security

## [1.5.3] - 2022-04-08

### Fixed

-	[NSIS]: do not remove dot `.` in output filename

## [1.5.2] - 2022-04-04

### Fixed

-	[NSIS]: secure output filename by remove spaces and any special characters
-	`New-Project` : fixed Parameter block

### Security

## [1.5.1] - 2022-04-01

### Added

-	handle `main` branch the same way as `master` branch
-	`fqVersion` new project attribute. It stands for Full Qualified Version. It compute accordingly PreRelease or Build numbers.

### Changed

### Deprecated

### Removed

### Fixed

-	single quotes are now allowed in project.yml files

### Security

## [1.5.0] - 2022-02-20

### Added

-	`PwSh.Fw.Windows.Cab` module to build Microsoft Cabinet archive
-	`New-ModuleManifestRecurse`: new function to simplify bulk creation of module manifest on a project. Simply use `New-ModuleManifestRecurse -Path /Path/to/Modules` and it will recurse through the path, find module and automatically use project's metadata merged with specific module.yml file as module metadata overrides if exist.
-	Option to override created setup/package filename. Use `-OutputFileName` to functions like `New-DebianBuild`, `New-CabinetFile` ou `New-NSISBuild`

### Changed

-	`Get-Project`: VERSION.BUILD number on tagged branches is build like on master branch
-	`Update-ModuleManifestEx` load and use ModuleName.yml file as metadata if it exist. It can be merged with global project's metadata, or ignored using `-PreserveMetadata` parameter
-	`Update-ModuleManifestRecurse` is renamed `Update-MetaModuleManifest` for better understanding. Old name is still active but marked as deprecated. It displays a warning.

### Deprecated

-	`Update-ModuleManifestRecurse`. Replaced by `Update-MetaModuleManifest`

### Removed

### Fixed

-	fixed icon handling

### Security

## [1.4.3] - 2021-07-23

### Added

-	2 new parameters for `Update-ModuleManifestEx` function : `-CreateOnly` to create module manifest ignoring existing ones, and `-UpdateOnly` to update existing manifest skipping creating missing ones.

### Changed

-	`ConvertTo-PowershellModuleSettings`: ignore Prelease attribute for old Powershell

### Deprecated

### Removed

### Fixed

-	`ConvertTo-PowershellModuleSettings`: Correctly translate Architecture for PowerShell module

### Security

## [1.4.2] - 2021-03-31

### Fixed

-   remove requirement to Powershell Core

## [1.4.1] - 2021-03-31

### Fixed

-   remove requirement to Powershell Core

## [1.4.0] - 2021-03-31

### Added

-	:scroll: `New-ProjectStructure`: new `Architecture` parameter. Default value is `All`.
-	:scroll: `Initialize-BuildTree`: new `-Force` parameter to allow merge multiple sources to the same destination and override existing files.

### Changed

-	`postinst.ps1` if exists is now executed by `pwsh.exe` (PowerShell Core)
-   remove requirement to Powershell Core

### Deprecated

### Removed

### Fixed

-	:scroll: `ConvertTo-WindowsIcon`: fixed use of correct variable to convert image
-	:scroll: `ConvertTo-WindowsIcon`: fixed copying existing icon file
-	:package: `PwSh.Fw.Build.Windows.NSIS`: fixed discovering `convert.exe` tool from [Image Magick](https://imagemagick.org/)
-	:package: `PwSh.Fw.Build.Windows.NSIS`: fixed icon path in registry
-	:package: `PwSh.Fw.Build.Windows.NSIS`: use `DisplayName` attribute if available

### Security

## [1.3.1] - 2021-01-12

### Fixed

-	:scroll: `New-ProjectStructure`: do not touch VERSION file if it already exists
-	:scroll: `New-ProjectStructure`: avoid new empty line at the end of VERSION file

## [1.3.0] - 2021-01-12

### Added

-	:scroll: `New-DesktopFile`: function to create linux desktop file
-	:scroll: `ConvertTo-LinuxIcons`, `ConvertTo-MacOSIcons` and `ConvertTo-WindowsIcon` functions to convert project's PNG icon file to suitable format

### Changed

### Deprecated

### Removed

### Fixed

-	:scroll: `New-NSISBuild`: fixed error when WindowsFolder does not exist
-	:scroll: `New-DebianField`: use converted control fields to build filename
-	:scroll: `Initialize-BuildTree`: fix files in buildMap no more copied to destination's parent folder
-	:scroll: `New-ProjectStructure`: fixed a typo that caused an error

### Security

## [1.2.1] - 2020-11-25

### Fixed

-	:scroll: `Initialize-BuildTree` explicitly create Destination build folder
-	:scroll: `Initialize-BuildTree` fixed empty buildMap value ( ex. "LICENSE" = "" ) not correctly handled

## [1.2.0] - 2020-11-25

### Added

-	new :scroll: `Initialize-BuildTree` to automate populate build tree
-	new :scroll: `New-ProjectStructure` to quickly bootstrap a new project
-	NSIS: execute `postinst.ps1` script if it exists

### Changed

### Deprecated

### Removed

### Fixed

-	:package: `PwSh.Build.Debian` fix architecture
-	:package: `PwSh.Build.Windows` fix icon in NSIS build

### Security

## [1.1.1] - 2020-06-25

### Changed

-	improved error messages
-	:scroll: `Update-ModuleManifestRecurse` now return `$null` if encounters an error.

### Deprecated

### Removed

### Fixed

### Security

## [1.1.0] - 2020-06-10

### Added

-	New sub-module :package: `PwSh.Fw.Build.Windows`. It can build NSIS setup.exe files from project
-	:package: `PwSh.Build.Debian` : new :scroll: `New-DebianBuild` function to handle building a debian package

## [1.0.1]

### Changed

-	:package: `PwSh.Fw.Build.Powershell` : fix creating / updating module manifest

## [1.0.0]

### Added

-	root module to manage project
-	Module to manage powershell modules
-	Module to manage Debian package build process
